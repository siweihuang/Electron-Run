const {app, BrowserWindow} = require('electron')

let mainWindow

function createWindow () {
    // Create the browser window.
    mainWindow = new BrowserWindow({
      // width: 1920, //100%
      // height: 1080,
      width: 1536, //80%
      height: 864,
      // width: 1152, //60%
      // height: 648,
      webPreferences: {
        nodeIntegration: true
      },
      frame: false
    })
  
    // and load the index.html of the app.
    // mainWindow.loadFile('index.html')
    mainWindow.loadURL(__dirname + '/index.html');
  
    // Open the DevTools.
    // mainWindow.webContents.openDevTools()
  
    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      mainWindow = null
    })
  
    // Disable menu
    var menu = null
    mainWindow.setMenu(menu)
  }

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) createWindow()
})

app.on('browser-window-created',function(event,window) {
  window.setMenu(null);
})