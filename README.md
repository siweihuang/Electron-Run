# electron-run

> This is sample code to open standalone Web Page using Chromium

## Build Setup

``` bash
# install dev dependencies
$ npm install

# Serve with hot reload at local Electron desktop application
npm run start

# Run for cloud [Part 1]: Modify the package.json and set the main attribute
"main": "cloud/main.js",

# Run for cloud [Part 2]: Further modify the cloud/main.js and specify the URL to be loaded
mainWindow.loadURL('https://www.google.com')

# Run for local HTML [Part 1]: Modify the package.json and set the main attribute
"main": "local/main.js",

# Run for local HTML [Part 2]: Copy and place all the UI code into the /local folder
# *Assume the main.js and renderer.js have already been defined
./local

```